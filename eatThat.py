""" Skrypt do pobierania dancyh o liczbie wystąpień
    brytyjskich słów z google ngrams """

import enchant  # słownik
import csv
import string  # małe litery alfabetu
import subprocess  # polecenia basha

alphabet = list(string.ascii_lowercase)

dct = enchant.Dict("en_GB")


def yearIndex(year):
        return year - 1800


def wordLenIndex(wordLen):
        return wordLen-1


name = "googlebooks-eng-gb-all-1gram-20120701-"
url = "http://storage.googleapis.com/books/ngrams/books/" + name
years = 2008-1800+1
maxWordLength = 184


for letter in list(string.ascii_lowercase):
    print(" --> Tworzę plik res_" + letter + ".csv")
    subprocess.call(["touch", "res_" + letter + ".csv"])
    print(" --> Rozpoczynam pobieranie 1gramu: " + letter)
    subprocess.call(["wget", url + letter + ".gz"])
    print(" --> Rozpakowywanie")
    subprocess.call(["gunzip", name + letter + ".gz"])
    print(" --> Rozpakowano")
    print(" --> Rozpoczynam sprawdzanie: ")
    with open("res_" + letter + ".csv", 'w') as f:
        writer = csv.writer(f)

        ngram = open("./" + name + letter, 'r')

        data = []
        for i in range(maxWordLength):
            data.append(years * [0])

        i = 0
        j = 0
        lastWord = ""
        lastWordInDict = False
        for line in ngram:
            i += 1
            l = line.split()
            # nowy wyraz taki jak poprzedni
            if l[0].lower() == lastWord:
                if (int(l[1]) > 1799) and (int(l[1]) < 2009) and lastWordInDict:
                    j += 1
                    data[wordLenIndex(len(l[0]))][yearIndex(int(l[1]))] += int(l[2])
                # jeśli poprzedniego wyrazu nie było w słowniku, albo rok zbyt
                # wczesny, to nie robimy nic
            # nowy wyraz inny niż poprzedni
            else:
                lastWord = l[0].lower()
                if dct.check(l[0]):
                    j += 1
                    lastWordInDict = True
                    if (int(l[1]) > 1799) and (int(l[1]) < 2009):
                        data[wordLenIndex(len(l[0]))][yearIndex(int(l[1]))] += int(l[2])
                else:
                    lastWordInDict = False

            if (i % 1000000 == 0):
                print(i)
        print(" --> Sprawdzanie zakończone: " + str(i) + " 1gramów, " + str(j) + " w słowniku.")

        writer.writerows(data)

    print(" --> Usuwam 1gram")
    subprocess.call(["rm", name + letter])
    print("Usunięto")
    print("Litera " + letter + ": zrobione")
